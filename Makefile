INSTALL=install
INSTALL_DATA=${INSTALL} -m 644
INSTALL_PROGRAM=$(INSTALL)

prefix=/usr/local
exec_prefix=$(prefix)
includedir=$(prefix)/include
bindir=$(exec_prefix)/bin

CC       = $(KOS_CC)
ARFLAGS  = rcs
CPPFLAGS = $(filter -D% -I%,$(KOS_CFLAGS))
CFLAGS   = $(filter-out -D% -I%,$(KOS_CFLAGS)) -Iinclude -std=gnu11 -Ofast
LDFLAGS  = $(KOS_LDFLAGS) $(KOS_LIB_PATHS)
LDLIBS   = -lglut -lEGL -lGL -lpng -lm -lz $(KOS_LIBS)

TARGET = kos_gl_png.elf
OBJS = main.o
CFLAGS += -std=gnu11 -Ofast

all: rm-elf $(TARGET)

include $(KOS_BASE)/Makefile.rules

clean:
	@$(RM) $(TARGET) $(OBJS) romdisk.*

rm-elf:
	@$(RM) $(TARGET) romdisk.*

$(TARGET): $(OBJS) romdisk.o
	$(KOS_CC) $(KOS_CFLAGS) $(KOS_LDFLAGS) -o $(TARGET) $(KOS_START) $(OBJS) romdisk.o $(OBJEXTRA) $(LDLIBS)

romdisk.img:
	$(KOS_GENROMFS) -f romdisk.img -d romdisk -v

romdisk.o: romdisk.img
	$(KOS_BASE)/utils/bin2o/bin2o romdisk.img romdisk romdisk.o

run: $(TARGET)
	$(KOS_LOADER) $(TARGET)

dist:
	rm -f $(OBJS) romdisk.o romdisk.img
	$(KOS_STRIP) $(TARGET)

