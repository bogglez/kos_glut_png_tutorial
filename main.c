#include <stdio.h>
#include <math.h>

#ifdef _arch_dreamcast
	#include <png/png.h>
	#include <kos.h>

	#define ASSETS_BASE "/rd/"
#else
	#include <png.h>
	#define GL_GLEXT_PROTOTYPES

	#define ASSETS_BASE "romdisk/"
#endif

#include <GL/glut.h>


#define CLEANUP(x) { ret = (x); goto cleanup; }


struct texture {
	GLuint id;
	GLenum format;
	GLenum min_filter;
	GLenum mag_filter;
	uint16_t w, h;
};

#ifdef _arch_dreamcast
	extern uint8 romdisk[];
	KOS_INIT_ROMDISK(romdisk);
#endif


struct texture tex;
static float const vertex_data[] = {
	/* 2D Coordinate + texture coordinate are the same */
	0.f, 1.f,
	0.f, 0.f,
	1.f, 1.f,
	1.f, 0.f,
};
static GLuint vao = 0;
static GLfloat transform[16];


static int png_to_gl_texture(struct texture * tex, char const * const filename) {
	int ret = 0;
	FILE * file = 0;
	uint8_t * data = 0;
	png_structp parser = 0;
	png_infop info = 0;
	png_bytep * row_pointers = 0;

	png_uint_32 w, h;
	int bit_depth;
	int color_type;


	if(!tex || !filename) {
		CLEANUP(1);
	}

	file = fopen(filename, "rb");
	if(!file) {
		puts("Cannot open file");
		CLEANUP(2);
	}

	parser = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
	if(!parser) {
		CLEANUP(3);
	}

	info = png_create_info_struct(parser);
	if(!info) {
		CLEANUP(4);
	}

	if(setjmp(png_jmpbuf(parser))) {
		CLEANUP(5);
	}

	png_init_io(parser, file);
	png_read_info(parser, info);
	png_get_IHDR(parser, info, &w, &h, &bit_depth, &color_type, 0, 0, 0);

	if((w & (w-1)) || (h & (h-1)) || w < 8 || h < 8) {
		CLEANUP(6);
	}

	if(png_get_valid(parser, info, PNG_INFO_tRNS) || (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8) || color_type == PNG_COLOR_TYPE_PALETTE) {
		png_set_expand(parser);
	}
	if(bit_depth == 16) {
		png_set_strip_16(parser);
	}
	if(color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA) {
		png_set_gray_to_rgb(parser);
	}

	int has_alpha = color_type & PNG_COLOR_MASK_ALPHA;
	if(!has_alpha) {
		int num_trans = 0;
		png_get_tRNS(parser, info, 0, &num_trans, 0);
		has_alpha = num_trans != 0;
	}

	png_read_update_info(parser, info);

	int rowbytes = png_get_rowbytes(parser, info);
	rowbytes += 3 - ((rowbytes-1) % 4); // align to 4 bytes

	data = malloc(rowbytes * h * sizeof(png_byte) + 15);
	if(!data) {
		CLEANUP(7);
	}

	row_pointers = malloc(h * sizeof(png_bytep));
	if(!row_pointers) {
		CLEANUP(8);
  }

	// set the individual row_pointers to point at the correct offsets of data
	for(png_uint_32 i = 0; i < h; ++i) {
		row_pointers[h - 1 - i] = data + i * rowbytes;
	}

	png_read_image(parser, row_pointers);

	// Generate the OpenGL texture object
	GLuint texture_id;
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	GLenum texture_format = (has_alpha) ? GL_RGBA : GL_RGB;
	glTexImage2D(GL_TEXTURE_2D, 0, texture_format, w, h, 0, texture_format, GL_UNSIGNED_BYTE, data);

	tex->id = texture_id;
	tex->w = w;
	tex->h = h;
	tex->format = texture_format;
	tex->min_filter = tex->mag_filter = GL_NEAREST;


cleanup:
	if(parser) {
		png_destroy_read_struct(&parser, info ? &info : 0, 0);
	}

	if(row_pointers) {
		free(row_pointers);
	}

	if(ret && data) {
		free(data);
	}

	if(file) {
		fclose(file);
	}

	return ret;
}

static void draw_textured_quad(struct texture const * const tex, float x0, float y0) {
	glBindTexture(GL_TEXTURE_2D, tex->id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, tex->min_filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, tex->mag_filter);

	glTranslatef(x0, y0, 0.f);
	glScalef(tex->w, tex->h, 1.f);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

static void draw() {
	static unsigned frame = 0;
	int x = (640 - tex.w) / 2;
	int y = (480 - tex.h) / 2;

	float bgcol = sinf(frame / 128.f);
	bgcol *= bgcol;
	glClearColor(bgcol, bgcol, bgcol, 1.f);
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadMatrixf(transform);
	draw_textured_quad(&tex, x, y);
	++frame;
	glutPostRedisplay();
	glutSwapBuffers();
}


int main(int argc, char **argv) {
	glutInit(&argc, argv); 
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA); 
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(-1, -1);
	if(glutCreateWindow("OpenGL PNG Tutorial") <= 0) {
		puts("Cannot create window.");
		return 1;
	}

	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);

	int ret = png_to_gl_texture(&tex, ASSETS_BASE "bg.png");
	if(ret) {
		printf("Cannot load texture, error code %d.\n", ret);
		return 1;
	}

	glGenVertexArrays(1, &vao);
	if(!vao) {
		puts("cannot generate vao");
		return 1;
	}
	glBindVertexArray(vao);

	GLuint vbo;
	glGenBuffers(1, &vbo);
	if(!vbo) {
		puts("cannot generate vbo");
		return 1;
	}

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer  (2, GL_FLOAT, 2 * sizeof(float), 0);
	glTexCoordPointer(2, GL_FLOAT, 2 * sizeof(float), 0);

	glBindVertexArray(0);

	glTranslatef(-1.f, -1.f, 0.f);
	glScalef(1.f/320.f, 1.f/240.f, 1.f);
	glGetFloatv(GL_MODELVIEW_MATRIX, transform);

	glutPostRedisplay();
	glutDisplayFunc(draw);
	glutMainLoop();

	return 0;
}
